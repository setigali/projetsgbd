package Ihm;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Services.ValiderActionCommand;
import Services.LoginActionCommand;

public class InterfaceAuthentification extends JFrame {
	
	private JPanel panel1;
	private JPanel panel2;
	private JPanel panel3;
	
	public static JTextField saisieLogin;
	
	public static JTextField saisieMotDePasse;
	
	
	private JLabel titreLogin;
	private JLabel titreMDP;
	
	private JButton boutonValider;
	//private JOptionPane aTester ; 
	
	
	public InterfaceAuthentification() throws HeadlessException {
		super();
		this.panel1 = new JPanel();
		this.panel1.setLayout(new GridLayout (2,4));
		this.panel2=new JPanel();
		this.panel2.setLayout(new GridLayout(1,1));;
		this.panel3=new JPanel();
		this.panel3.setLayout(new GridLayout(2,1));
		this.saisieLogin = new JTextField();
	
		this.saisieMotDePasse = new JTextField();
		this.titreLogin = new JLabel("Login");
		this.titreMDP = new JLabel("Mot de passe");
		this.boutonValider= new JButton("Valider");
		boutonValider.addActionListener(new ValiderActionCommand()); 
			
			
		this.setTitle("Authentification");
		
		panel1.add(titreLogin);
		panel1.add(saisieLogin);
		panel1.add(titreMDP);
		panel1.add(saisieMotDePasse);
		
		panel2.add(boutonValider);
		
		panel3.add(panel1);
		panel3.add(panel2);
		
		this.add(panel3);
		
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	
	

}
