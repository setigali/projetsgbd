package Ihm;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import Services.ExecuterActionCommand;
import Services.LectureEcritureTable;

public class Interface extends JFrame {

	public static InterfaceAuthentification copieInterface;
	private JPanel panel1;
	private JPanel panel2;
	private JPanel panel3;
	private JPanel panel4;
	private JPanel panel5;

	public static JList listeTable;

	public static JTextArea commandeSql;
	private JTextArea resultat;

	private JButton boutonExecuter;

	private JLabel titreResultat;

	private JMenuBar barreMenu;

	private JMenu Fichier;

	private JMenuItem Reset;
	private JMenuItem ChangerDeBD;
	private JMenuItem Quitter;

	private GridLayout grid1;
	private GridLayout grid2;
	private GridLayout grid3;
//	private GridLayout grid4;

	public Interface() throws HeadlessException {
		super();
		this.setTitle("CDA SGBD");
		this.setSize(1500, 1000);
		this.setResizable(true);

		this.panel1 = new JPanel();
		this.panel2 = new JPanel();
		this.panel3 = new JPanel();
		this.panel4 = new JPanel();
		// this.panel5 = new JPanel();

		this.listeTable = new JList();

		this.commandeSql = new JTextArea();
		this.resultat = new JTextArea();
		this.resultat.setText(
				"+----------+-------------+----------+--------------+\n| Nom      | Prenom       |   Numtel   |    Email      |\n+----------+-------------+----------+--------------+\n");

		this.boutonExecuter = new JButton("Executer");
		this.boutonExecuter.addActionListener(new ExecuterActionCommand());
		boutonExecuter.setSize(2, 1);

		this.titreResultat = new JLabel("Resultat");

		this.barreMenu = new JMenuBar();
		this.Fichier = new JMenu();
		this.ChangerDeBD = new JMenuItem();
		this.Quitter = new JMenuItem();
		this.Reset = new JMenuItem();

		this.grid1 = new GridLayout(2, 1);
		this.grid2 = new GridLayout(1, 2, 25, 15);
		this.grid3 = new GridLayout(2, 1, 15, 15);

		barreMenu.add(Fichier).setText("Fichier");
		Fichier.add(ChangerDeBD).setText("Changer de BD");
		Fichier.add(Reset).setText("Reset");
		Fichier.add(Quitter).setText("Quitter");

		panel1.setLayout(grid1);

		panel2.setLayout(grid2);
		panel1.add(panel2);

		/*
		 * String tab[]= new String [2]; tab[0] = "CDA"; tab[1] = "Hello";
		 */

		panel3.setLayout(grid3);
		panel3.add(commandeSql);
		panel3.add(boutonExecuter);

		panel4.setLayout(grid1);
		panel4.add(titreResultat);
		panel4.add(resultat);

		panel1.add(panel4);
		listeTable.setListData(LectureEcritureTable.listeTable(new File("C:\\ENV\\BDD")));
		panel2.add(listeTable);

		panel2.add(panel3);

		resultat.setEditable(false);

		this.setJMenuBar(barreMenu);
		this.add(panel1);

		this.pack();
		this.setVisible(true);

		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		copieInterface = new InterfaceAuthentification();

	}
}
