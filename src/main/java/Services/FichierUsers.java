package Services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FichierUsers {

	public void lectureFichier() {

		try {

			FileReader lecteur;
			lecteur = new FileReader("FichierUsers.txt");

			BufferedReader br = new BufferedReader(lecteur);

			try {
				while (br.ready())

					System.out.println(br.readLine());
				br.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}

		

		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("Erreur" + e);
		}
	}

	public void ecritureFichier() {

		try {
			FileWriter ecrire = new FileWriter("FichierUsers.txt",true);
			
			BufferedWriter bw = new BufferedWriter(ecrire);
			bw.write("Benjira;test;123");
			bw.newLine();
			bw.write("Larry;cda;123");
			bw.newLine();
			
			
			bw.close();
		}

		catch (Exception e) {
			System.out.println("Erreur " + e);
		}
	}

}
